# Config

## What can you find here

- Clone this repo.
- Then ```cd``` to this repo and either ```cat file``` or just look for it ```ls -a```.
- Note that the . files are secret files since it starts with dot. So just ```ls``` will not display it.
- Note you must be using ZSH (for .zshrc) if you are using Bash it will not work for you.

