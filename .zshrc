# MUST BE LINKED TO THE /home/adam DIRECTORY


# Use powerline
USE_POWERLINE="true"

# Enable vcs_info
setopt promptsubst
autoload -Uz vcs_info

autoload -Uz compinit
compinit

# Automatically change directory when command is entered
setopt auto_cd

# Define function to update version control status
precmd () { vcs_info }

# Set TEXMFHOME to the path of your personal texmf directory
export TEXMFHOME=~/texmf

# Set TEXMFLOCAL to the path of the system-wide texmf directory
export TEXMFLOCAL=/usr/local/share/texmf

export LC_ALL=en_US.utf8

# Recompute every prompt
setopt promptsubst

# Highlighting
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Adding "":/home/adam/bin" to PATH
export PATH="$PATH:/home/adam/bin"

# Load git stuff
zstyle ':vcs_info:*' formats ' %F{2}%s(%F{100}%r%F{2}: %F{104}%b%F{2})'

# Git
gits='${vcs_info_msg_0_}'

# Function to set colors for each character
function colored_prompt {
    local a="%F{196}A%f"  # Red
    local d="%F{202}d%f"  # Orange
    local a_acute="%F{220}á%f"  # Yellow
    local m="%F{46}m%f"   # Green
    local e="%F{39}e%f"   # Blue
    local k="%F{201}k%f"  # Pink

    echo -e "${a}${d}${a_acute}${m}${e}${k}"
}


# Prompt
PS1="$(colored_prompt) %F{7}%/%f%b$gits%F{187}%F{196} %(?..%?)%f
 %F{2}>%F{7} "

# Aliases
alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias ls="eza"
alias fastpush='git add .; git commit -m "..."; git push'
alias wifi="nmtui"

# Autosuggestions
source /home/adam/.zsh/zsh-autosuggestions/

# Bindkeys for ctrl skipping words and deleting
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^H" backward-kill-word

# Enable partial history search
autoload -Uz history-search-end
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward

# History settings
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt HIST_IGNORE_SPACE
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS

# Created by `pipx` on 2024-04-07 11:02:50
export PATH="$PATH:/home/adam/.local/bin"

