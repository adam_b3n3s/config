#!/bin/bash

# Function to handle interruption signal
trap 'exit' SIGINT

# Set initial time for WiFi information retrieval
wifi_info_time=0

while true; do
    # Get volume using pactl
    volume_info=$(pactl list sinks)
    volume=$(echo "$volume_info" | grep 'Volume:' | head -n 1 | awk '{print $5}')
    mute_status=$(echo "$volume_info" | grep 'Mute:' | awk '{print $2}')

    # Get battery percentage and state using acpi
    battery_info=$(acpi)
    battery_percentage=$(echo "$battery_info" | awk '{print $4}' | tr -d '%,')
    battery_state=$(echo "$battery_info" | awk '{print $3}' | sed 's/,//')

    # Correct battery state if it's "charging" or "not charging"
    if [[ "$battery_state" == "Not" ]]; then
        battery_state="Not charging"
        battery_percentage=$(echo "$battery_info" | awk '{print $5}' | tr -d '%,')
    fi

    # Get current time and date in EU format (24-hour clock)
    current_time=$(date +"%H:%M")
    current_date=$(date +"%d.%m.%Y")

    # Get wifi connection information if it's time to update
    current_timestamp=$(date +"%s")
    if [ "$current_timestamp" -ge "$wifi_info_time" ]; then
        wifi_connection=$(nmcli -t -f active,ssid dev wifi | grep '^yes' | cut -d':' -f2)
        # Update next retrieval time by adding a delay (in this case, 60 seconds)
        wifi_info_time=$((current_timestamp + 20))
    fi

		# Get the current keyboard layout
		layout=$(setxkbmap -query | grep layout | awk '{print $2}')

		# Capitalize the layout
		layout_capitalized=$(echo $layout | tr '[:lower:]' '[:upper:]')

    # Construct output string with separators
    output="Vol: $volume"
    if [ "$mute_status" = "yes" ]; then
        output+=" (Muted)"
    fi

    output+=" | Bat: $battery_percentage% ($battery_state) | Date: $current_date | Time: $current_time | Wifi: $wifi_connection | $layout_capitalized "

    # Print output
    echo "$output"

    # Adjust sleep time as needed
    sleep 0.4
done

