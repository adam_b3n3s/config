#!/bin/bash

current_brightness=$(xrandr --verbose | grep -i 'Brightness:' | awk '{print $2}')
new_brightness=$(echo "$current_brightness - 0.1" | bc)

if (( $(echo "$new_brightness < 0.1" | bc -l) )); then
    new_brightness=0.0
fi

xrandr --output eDP-1 --brightness $new_brightness

