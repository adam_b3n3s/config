" Set compatibility to Vim only
set nocompatible

" Enable Ctrl + Backspace to delete whole words in insert mode
inoremap <C-H> <C-W>

" Enable syntax highlighting
syntax on

" Toggle line numbers
nnoremap <C-n> :call ToggleLineNumbers()<CR>

function! ToggleLineNumbers()
    if(&number == 1)
        set nonumber
    else
        set number
    endif
endfunction

" Set line numbers by default
set number

" Tabs len = 2
set tabstop=2

" Status bar
set laststatus=2

